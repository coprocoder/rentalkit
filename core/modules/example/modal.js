/** https://github.com/andyzack/custom-modal
 * <div className="page">
 *     <section>
 *         <h1>Main Heading</h1>
 *         <sitemate-modal>
 *             <h3 slot="header">Main Confirmation Message</h3>
 *             <p slot="message">Are you sure you want to continue?</p>
 *         </sitemate-modal>
 *     </section>
 * </div>
 */

import css from '!!css-loader!sass-loader!./modal.scss'

class CustomModal extends HTMLElement {
    _modalVisible = false;
    _modal;

    constructor() {
        super();
        this.attachShadow({mode: 'open'});
        this.shadowRoot.innerHTML = `
		<style>${css}</style>
		<div class="modal-results"></div>
		<button class="open">Open Modal</button>
		<div class="modal">
			<div class="modal-content">
				<div class="modal-header">
					<slot name="header"><h3>Default Heading</h3></slot>
				</div>
				<div class="modal-body">
					<slot name="message">Default Text</slot>
				</div>
				<button class="submit">Yes</button><button class="close">Cancel</button>
			</div>
		</div>
		`
    }

    connectedCallback() {
        this._modal = this.shadowRoot.querySelector(".modal");
        this.shadowRoot.querySelector("button").addEventListener('click', this._showModal.bind(this));
        this.shadowRoot.querySelector(".close").addEventListener('click', this._hideModal.bind(this));
        this.shadowRoot.querySelector(".submit").addEventListener('click', this._submitModal.bind(this));
    }

    disconnectedCallback() {
        this.shadowRoot.querySelector("button").removeEventListener('click', this._showModal);
        this.shadowRoot.querySelector(".close").removeEventListener('click', this._hideModal);
        this.shadowRoot.querySelector(".submit").removeEventListener('click', this._submitModal);
    }

    _showModal() {
        this._modalVisible = true;
        this._modal.style.display = 'block';
    }

    _hideModal() {
        this._modalVisible = false;
        this._modal.style.display = 'none';
        this.shadowRoot.querySelector(".modal-results").innerHTML = '<span style="color:#d9534f">You just click "Cancel"</span>';
    }

    _submitModal() {
        this._modalVisible = false;
        this._modal.style.display = 'none';
        this.shadowRoot.querySelector(".modal-results").innerHTML = '<span style="color:#2e81da">You just click "Yes"</span>';
    }
}

customElements.define('sitemate-modal', CustomModal);